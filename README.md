# CityApp
## _One Stop solution for Object Detection in VR_




Cityapp is a unity based, VR only app for object detection
The internal inference engine is powered by Tensorflow Lite
Pretrained model has been used for inference

Model Trained on
- COCO Dataset
- CityScapes


## Features

- It's as simple as plug and play
- Just open in your phone and your're good to go
- Point at something and get your results within 'milli'- seconds

## Perfomence

- Tested on SnapDragon 835 On Android 11 with latest Carboard SDK
- Presence of a vislble lag in Bouding box generation
- Restriction due to device hardware

## Sample

![](sample.jpeg)
